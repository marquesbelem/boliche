﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Controller : MonoBehaviour
{
    public static Controller _intance = null;

    [SerializeField]
    GameObject painel_instrucao;
    [SerializeField]
    GameObject painel_pontuacao;

    Jogada jogada;
    public bool lancamento_cronometro;
    float cronometro_lm = 0;

    [SerializeField]
    Text[] text_lacamento_um;

    [SerializeField]
    Text[] text_lacamento_dois;

    [SerializeField]
    Text[] text_total_rodada;

    GameObject[] pinos;

    void Awake()
    {
        if (_intance == null)
            _intance = this;
    }

    void Start()
    {
        painel_instrucao.SetActive(true);
        painel_pontuacao.SetActive(false);
        jogada = FindObjectOfType<Jogada>();
        jogada.enabled = false;

        pinos = GameObject.FindGameObjectsWithTag("Pino");
    }

    void Update()
    {

        if (lancamento_cronometro)
            CronometroLancamento();
    }

    public void Iniciar()
    {
        painel_instrucao.SetActive(false);
        painel_pontuacao.SetActive(true);
        JogarBola._intance.enabled = true;
    }

    void CronometroLancamento()
    {
        cronometro_lm += Time.deltaTime;
        
        if (cronometro_lm > 1.5f)
        {
            ResetTentativa();
        }
    }

   public void ResetTentativa()
    {
        text_lacamento_um[Jogada._intance.rodada - 1].text = Jogada._intance.ptcao_lanca_um[Jogada._intance.rodada - 1].ToString();
        text_lacamento_dois[Jogada._intance.rodada - 1].text = Jogada._intance.ptcao_lanca_dois[Jogada._intance.rodada - 1].ToString();
        text_total_rodada[Jogada._intance.rodada - 1].text = Jogada._intance.resultado_final().ToString();
        
        if (Jogada._intance.lacamento < 2)
        {
            Jogada._intance.lacamento++;
        }
        else
        {
            Jogada._intance.lacamento = 1;
            Jogada._intance.rodada++;
        }
        
        lancamento_cronometro = false;
        cronometro_lm = 0;
        JogarBola._intance.ResetBola();

        for (int i = 0; i < pinos.Length; i++)
        {
            pinos[i].GetComponent<Pino>().ResetPinos();
        }


        if (Jogada._intance.rodada == Jogada._intance.qtd_rodada)
        {
            SceneManager.LoadScene("SampleScene");
        }
    }
}
