﻿using UnityEngine;

public class Jogada : MonoBehaviour
{
    public static Jogada _intance = null;
    public int rodada = 1;

    public int qtd_rodada = 4;
    public int lacamento = 1;

    //Pontuacao por laçamento
    public int[] ptcao_lanca_um;
    public int[] ptcao_lanca_dois;
    
    //Resultado dos dois laçamentos
    public int[] ptcao_final;

    [SerializeField]
    int ptcao_final_geral;
    void Awake()
    {
        if (_intance == null)
            _intance = this;
    }

    void Start()
    {
        ptcao_lanca_um = new int[qtd_rodada];
        ptcao_lanca_dois = new int[qtd_rodada];
        ptcao_final = new int[qtd_rodada];
    }
    public int resultado_final()
    {
        ptcao_final[rodada - 1] = ptcao_lanca_um[rodada - 1] + ptcao_lanca_dois[rodada - 1];
        return ptcao_final[rodada - 1];
    }
    

}
