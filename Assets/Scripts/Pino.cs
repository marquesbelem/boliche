﻿using UnityEngine;

public class Pino : MonoBehaviour
{
    public static Pino _intance = null;

    Quaternion rotation_inicial;
    [SerializeField]
    public bool caiu = false;

    Vector3 transform_inicial;
    Rigidbody rig;

    void Awake()
    {
        if (_intance == null)
            _intance = this;
    }

    void Start()
    {
        rig = this.GetComponent<Rigidbody>();
        rotation_inicial = this.transform.rotation;
        transform_inicial = this.transform.position;
    }


    void Update()
    {
        getDerrubou();

        if (caiu)
        {
            if(Jogada._intance.lacamento == 1)
            {
                Jogada._intance.ptcao_lanca_um[Jogada._intance.rodada - 1]++;
                caiu = false;
                this.enabled = false;
            }

            if (Jogada._intance.lacamento == 2)
            {
                Jogada._intance.ptcao_lanca_dois[Jogada._intance.rodada - 1]++;
                caiu = false;
                this.enabled = false;
            }
        }
    }

    //Verifica se o pino caiu
    public void getDerrubou()
    {
        if (this.transform.rotation.x != rotation_inicial.x ||
            this.transform.rotation.y != rotation_inicial.y)
        {
            caiu = true;
        }
    }

    public void ResetPinos()
    {
        this.enabled = true;
        this.transform.rotation = rotation_inicial;
        this.transform.position = transform_inicial;
        rig.Sleep();
    }
}
