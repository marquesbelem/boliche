﻿using UnityEngine;

public class JogarBola : MonoBehaviour
{
    public static JogarBola _intance = null;

    [SerializeField]
    float forca = 1000;

    [SerializeField]
    float velocidade1 = 10;
    [SerializeField]
    float velocidade2 = 10;

    Rigidbody rig;
    public bool jogou = true;
    public bool acertou_pinos = false;

    Vector3 transform_inicial;
    Quaternion rotation_inicial;

    void Awake()
    {
        if (_intance == null)
            _intance = this;
    }

    void Start()
    {
        rig = this.GetComponent<Rigidbody>();
        this.enabled = false;

        transform_inicial = this.transform.position;
        rotation_inicial = this.transform.rotation;
    }


    void Update()
    {
        if (jogou)
        {
            rig.freezeRotation = false;
            //Lança a bola
            if (Input.GetAxis("Jump") != 0 || Input.GetAxis("Fire1") != 0)
            {
               
                rig.AddForce(forca * transform.forward);
                jogou = false;
            }

            //Direção em relação a rotação
            float direcao_seta = Input.GetAxis("Mouse X") * velocidade1;
            direcao_seta *= Time.deltaTime;
            transform.Rotate(0, direcao_seta, 0);

            //Direção em relação ao eixo X 
            float direcao = Input.GetAxis("Horizontal") * velocidade2;
            if (Input.GetAxis("Horizontal") != 0)
            {
                direcao *= Time.deltaTime;
                transform.Translate(direcao, 0, 0);
            }
        }
    }

    void OnCollisionExit(Collision col)
    {
        if (col.gameObject.CompareTag("Pista"))
        {
            Controller._intance.ResetTentativa();
        }

    }

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.CompareTag("Pino"))
        {
            Controller._intance.lancamento_cronometro = true;
        }
    }

    public void ResetBola()
    {
        this.transform.position = transform_inicial;
        this.transform.rotation = rotation_inicial;
        rig.Sleep();
        rig.freezeRotation = true;
        jogou = true;

    }
}
